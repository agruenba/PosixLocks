from dataclasses import dataclass
import LockRanges

DLM_PLOCK_OP_LOCK = 1
DLM_PLOCK_OP_UNLOCK = 2
DLM_PLOCK_OP_GET = 3

# Declaring classes RequestKey and Request as @dataclass gives us an automatic
# implementation of __repr__.

@dataclass(unsafe_hash=True)
class RequestKey:
    nodeid: int
    owner: int

@dataclass(eq=False)
class Request(LockRanges.Request):
    pid: int
    optype: int
    wait: bool
    inception: int | None = None
    conflict: int | None = None

    def __init__(self, nodeid, owner, pid, start, end, ex, optype, wait):
        LockRanges.Request.__init__(self, nodeid, owner, start, end, ex)
        self.pid = pid
        self.optype = optype
        self.wait = wait

    def key(self):
        return RequestKey(self.nodeid, self.owner)

class Resource(LockRanges.Resource):
    requests: dict[RequestKey, Request] = dict()

    def add_request(self, request):
        self.requests[request.key()] = request

    def request(self, nodeid: int, owner: int):
        return self.requests[RequestKey(nodeid, owner)]

    def drop_request(self, request):
        del self.requests[request.key()]

class Filesystem:
    # resources by number (i.e., inodes by inode number)
    resources: dict[int, Resource] = dict()

class Locks:
    # filesystems by fsid
    filesystems: dict[int, Filesystem] = dict()

    def resource(self, fsid: int, number: int) -> Resource:
        try:
            filesystem = self.filesystems[fsid]
        except KeyError:
            filesystem = Filesystem()
            self.filesystems[fsid] = filesystem

        try:
            resource = filesystem.resources[number]
        except KeyError:
            resource = Resource()
            filesystem.resources[number] = resource

        return resource

    def __iter__(self):
        for fsid in self.filesystems:
            filesystem = self.filesystems[fsid]
            for number in filesystem.resources:
                resource = filesystem.resources[number]
                for lock in resource.locks:
                    yield (fsid, number, lock)

    def lock(self, fsid: int, number: int, request: LockRanges.LockRequest):
        resource = self.resource(fsid, number)
        resource.lock(request)

    def unlock(self, fsid: int, number: int, request: LockRanges.UnlockRequest):
        resource = self.resource(fsid, number)
        resource.unlock(request)

__all__ = ['Locks', 'Request', 'DLM_PLOCK_OP_LOCK', 'DLM_PLOCK_OP_UNLOCK',
           'DLM_PLOCK_OP_GET']
