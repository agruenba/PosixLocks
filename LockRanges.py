from dataclasses import dataclass

@dataclass(eq=False)
class Request:
    nodeid: int
    owner: int
    start: int
    end: int
    ex: bool

class UnlockRequest(Request):
    def __init__(self, nodeid, owner, start, end):
        Request.__init__(self, nodeid, owner, start, end, False)

class LockRequest(Request):
    def __init__(self, nodeid, owner, start, end, ex):
        Request.__init__(self, nodeid, owner, start, end, ex)

class Lock(Request):
    def shrink_range(self, start: int, end: int):
        (self.start, self.end) = shrink_range2(self.start, self.end, start, end)

    # No __eq__() and __hash__() implementations, so set membership is
    # determined by object identity.

class Resource:
    # Python doesn't allow modifying collections like sets and lists while
    # iterating over them, which is what the lock() and unlock() operations
    # would like to do.  Using a linked list would solve this problem, but
    # linked lists cannot be implemented cleanly in Python.  To get around
    # that, we create a set of objects to add (locks_to_add) and a set of
    # objects to remove (locks_to_remove), and we add / remove the locks in
    # those sets once we're done iterating the set of locks.

    locks: set[Lock] = set()
    locks_to_add: set[Lock] = set()
    locks_to_remove: set[Lock] = set()

    def prepare_add_lock(self, lock):
        self.locks_to_add.add(lock)

    def prepare_remove_lock(self, lock):
        self.locks_to_remove.add(lock)

    def commit(self):
        for lock in self.locks_to_remove:
            self.locks.remove(lock)
        self.locks_to_remove.clear()
        for lock in self.locks_to_add:
            self.locks.add(lock)
        self.locks_to_add.clear()

    def prepare_lock_case1(self, lock: Lock, req: Request):
        # RN within RE (and starts or ends on RE boundary)
        # 1. add new lock for non-overlap area of RE, orig mode
        # 2. convert RE to RN range and mode

        (start2, end2) = shrink_range2(lock.start, lock.end, req.start, req.end)

        # non-overlapping area start2:end2
        (lock.start, lock.end) = (req.start, req.end)
        lock.ex = req.ex

        self.prepare_add_lock(Lock(req.nodeid, req.owner,
                                   start2, end2, not req.ex))

    def prepare_lock_case2(self, lock: Lock, req: Request):
        # RN within RE (RE overlaps RN on both sides)
        # 1. add new lock for front fragment, orig mode
        # 2. add new lock for back fragment, orig mode
        # 3. convert RE to RN range and mode */

        self.prepare_add_lock(Lock(req.nodeid, req.owner,
                                   lock.start, req.start - 1, not req.ex))
        self.prepare_add_lock(Lock(req.nodeid, req.owner,
                                   req.end + 1, lock.end, not lock.ex))
        (lock.start, lock.end) = (req.start, req.end)
        lock.ex = req.ex

    def prepare_lock(self, req: Request):
        for lock in self.locks:
            if lock.nodeid != req.nodeid or lock.owner != req.owner:
                continue
            if not ranges_overlap(lock.start, lock.end, req.start, req.end):
                continue

            # existing range (RE) overlaps new range (RN)
            overlap = overlap_type(req.start, req.end, lock.start, lock.end)

            # ranges the same
            if overlap == 0:
                if lock.ex == req.ex:
                    return
                lock.ex = req.ex
                return

            # RN within RE and starts or ends on RE boundary
            if overlap == 1:
                if lock.ex == req.ex:
                    return
                self.prepare_lock_case1(lock, req)
                return

            # RN within RE
            if overlap == 2:
                if lock.ex == req.ex:
                    return
                self.prepare_lock_case2(lock, req)
                return

            # RE within RN
            if overlap == 3:
                self.prepare_remove_lock(lock)
                pass

            # front of RE in RN, or end of RE in RN
            if overlap == 4:
                if lock.start < req.start:
                    lock.end = req.start - 1
                else:
                    lock.start = req.end + 1
                pass

        self.prepare_add_lock(Lock(req.nodeid, req.owner,
                                   req.start, req.end, req.ex))

    def lock(self, request: Request):
        self.prepare_lock(request)
        self.commit()

    def prepare_unlock(self, req: Request):
        for lock in self.locks:
            if lock.nodeid != req.nodeid or lock.owner != req.owner:
                continue
            if not ranges_overlap(lock.start, lock.end, req.start, req.end):
                continue

            # existing range (RE) overlaps new range (RN)
            overlap = overlap_type(req.start, req.end, lock.start, lock.end)

            # ranges the same -
            # just remove the existing lock
            if overlap == 0:
                self.prepare_remove_lock(lock)
                return

            # RN within RE and starts or ends on RE boundary -
            # shrink and update RE
            if overlap == 1:
                lock.shrink_range(req.start, req.end)
                return

            # RN within RE -
            # shrink and update RE to be front fragment, and add a new lock for
            # back fragment
            if overlap == 2:
                self.prepare_add_lock(Lock(req.nodeid, req.owner,
                                           req.end + 1, lock.end, lock.ex))
                lock.end = req.start - 1
                return

            # RE within RN -
            # remove RE, then continue checking because RN could cover other
            # locks
            if overlap == 3:
                self.prepare_remove_lock(lock)
                continue

            # front of RE in RN, or end of RE in RN -
            # shrink and update RE, then continue because RN could cover other
            # locks
            if overlap == 4:
                lock.shrink_range(req.start, req.end)
                continue

    def unlock(self, request: Request):
        self.prepare_unlock(request)
        self.commit()

    def is_conflict(self, req: Request):
        for lock in self.locks:
            if lock.nodeid == req.nodeid and lock.owner == req.owner:
                continue
            if not ranges_overlap(lock.start, lock.end, req.start, req.end):
                continue;
            if lock.ex or req.ex:
                return True
        return False

def ranges_overlap(start1: int, end1: int, start2: int, end2: int) -> bool:
    return end1 >= start2 and start1 <= end2

# @start1 - start of new lock range
# @end1 - end of new lock range
# @start2 - start of existing lock range
# @end2 - end of existing lock range
def overlap_type(start1: int, end1: int, start2: int, end2: int) -> int:
    # ---r1---
    # ---r2---
    if start1 == start2 and end1 == end2:
        return 0

    # --r1--
    # ---r2---
    if start1 == start2 and end1 < end2:
        return 1

    #   --r1--
    # ---r2---
    if start1 > start2 and end1 == end2:
        return 1

    #  --r1--
    # ---r2---
    if start1 > start2 and end1 < end2:
        return 2

    # ---r1---  or  ---r1---  or  ---r1---
    # --r2--	  --r2--       --r2--
    if start1 <= start2 and end1 >= end2:
        return 3

    #   ---r1---
    # ---r2---
    if start1 > start2 and end1 > end2:
        return 4

    # ---r1---
    #   ---r2---
    if start1 < start2 and end1 < end2:
        return 4

    raise RuntimeError("interval not overlapping")

# shrink the range start2:end2 by the partially overlapping start:end
def shrink_range2(start2: int, end2: int,
                  start: int, end: int) -> tuple[int, int]:
    if start2 < start:
        return (start2, start - 1)
    elif end2 > end:
        return (end + 1, end2)
    else:
        raise RuntimeError("interval not overlapping")
