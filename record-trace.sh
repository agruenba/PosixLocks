#! /bin/sh

set -e

run() {
    echo "$*"
    "$@"
}

# Only when cluster is already running:
#our_nodeid=$(corosync-cfgtool -s | sed -ne 's:Local node ID \([0-9]\+\).*:\1:p')

run perf record \
    -e probe:gfs2_sys_fs_add \
    -e 'probe:dlm_posix_*' \
    -e 'sdt_dlm_controld:global_*' \
    -e 'sdt_dlm_controld:plock_*' \
    -e gfs2:gfs2_glock_state_change \
    -e gfs2:gfs2_log_flush
