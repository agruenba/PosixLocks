#! /bin/sh


awk '
BEGIN {
	last_sec = 0
	split("", i2c) # clear array
	split("", c2i) # clear array
	num_inodes = 0
	split("", count) # clear array
}

function report() {
	if (last_sec) {
		printf "%s", last_sec
		for (idx = 1; idx <= num_inodes; idx++)
			for (op = 1; op <= 3; op++)
				printf " %s", count[idx,op]
		printf "\n"
	}
	for (idx = 1; idx <= num_inodes; idx++) {
		for (op = 1; op <= 3; op++)
			count[idx,op] = 0
	}
}

function record(timestamp, inode, op) {
	sec = int(timestamp)
	if (last_sec != sec) {
		report()
		last_sec = sec
	}
	if (!(inode in i2c)) {
		num_inodes++
		i2c[inode] = num_inodes
		c2i[num_inodes] = inode
	}
	idx = i2c[inode]
	if (!((idx,1) in count)) {
		count[idx,1] = 0
		count[idx,2] = 0
		count[idx,3] = 0
	}
	count[idx,op]++
}

$5 == "dlm_posix_get" {
	record($1, $3, 1)
}

$5 == "dlm_posix_lock" {
	record($1, $3, 2)
}

$5 == "dlm_posix_unlock" {
	record($1, $3, 3)
}

END {
	if (last_sec)
		report()
	printf "inode"
	for (idx = 1; idx <= num_inodes; idx++) {
		inode = c2i[idx]
		printf " %s %s %s", inode, inode, inode
	}
	printf "\n"
	printf "op"
	for (idx = 1; idx <= num_inodes; idx++) {
		printf " get lock unlock"
	}
	printf "\n"
}
'
