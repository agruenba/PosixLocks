#! /usr/bin/python3

import sys
import re
import LockRanges

resource = LockRanges.Resource()

for line in sys.stdin:
    line = re.sub(r"#.*", "", line)
    args = line.split()
    if len(args) == 0:
        continue

    # lock nodeid owner start end ex
    if args[0] == "lock":
        if len(args) != 6:
            raise RuntimeError("wrong number of arguments")
        args = list(map(int, args[1:]))
        args[4] = bool(args[4])
        request = LockRanges.LockRequest(*args)
        resource.lock(request)

    # unlock nodeid owner start end
    elif args[0] == "unlock":
        if len(args) != 5:
            raise RuntimeError("wrong number of arguments")
        args = list(map(int, args[1:]))
        request = LockRanges.UnlockRequest(*args)
        resource.unlock(request)

    else:
        raise RuntimeError("unknown operation")

for lock in resource.locks:
    print("lock", lock.nodeid, lock.owner, lock.start, lock.end, int(lock.ex))
