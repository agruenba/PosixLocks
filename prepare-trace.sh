#! /bin/sh

set -e

unprobe() {
    for probe in "$@"; do
	perf probe --quiet --del "$probe" || :
    done
}

probe() {
    if ! perf probe --quiet "$@"; then
	echo perf probe "$@"
	perf probe "$@" || exit $?
    fi
}

modprobe dlm
modprobe gfs2

# device number to filesystem name mapping
unprobe probe:gfs2_sys_fs_add
probe -m gfs2 --add='gfs2_sys_fs_add dev=sdp->sd_vfs->s_dev fsname=sdp->sd_fsname:string'

# dlm upcalls
# (Note the fixed line numbers in the tracesoints; pattern-matching isn't possible for tracepoints that have arguments.)
unprobe probe:dlm_posix_lock_L12 probe:dlm_posix_lock__return
probe -m dlm --add='dlm_posix_lock:12 pid=fl->fl_pid:u owner=fl->fl_owner:u fsid=ls->ls_global_id:u dev=file->f_inode->i_sb->s_dev number=number:u start=fl->fl_start:u end=fl->fl_end:u cmd=cmd type=fl->fl_type:u'
probe -m dlm --add='dlm_posix_lock%return rv=$retval'

unprobe probe:dlm_posix_unlock_L12 probe:dlm_posix_unlock__return
probe -m dlm --add='dlm_posix_unlock:12 pid=fl->fl_pid:u owner=fl->fl_owner:u fsid=ls->ls_global_id:u dev=file->f_inode->i_sb->s_dev:u number=number:u start=fl->fl_start:u end=fl->fl_end:u'
probe -m dlm --add='dlm_posix_unlock%return rv=$retval'

unprobe probe:dlm_posix_get_L11 probe:dlm_posix_get__return
probe -m dlm --add='dlm_posix_get:11 pid=fl->fl_pid:u owner=fl->fl_owner:u fsid=ls->ls_global_id:u dev=fl->fl_file->f_inode->i_sb->s_dev:u number=number:u start=fl->fl_start:u end=fl->fl_end:u'
probe -m dlm --add='dlm_posix_get%return rv=$retval'

# dlm_controld tracepoints
unprobe 'sdt_dlm_controld:*'
probe -x /usr/sbin/dlm_controld 'sdt_dlm_controld:*'
